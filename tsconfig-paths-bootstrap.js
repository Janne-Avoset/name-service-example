const tsConfigPaths = require('tsconfig-paths');

/** This is used to resolve absolute imports in built project
 *  eg. `import X from 'src/something'` is compiled into `const X = require('src/something')`
 *  and this bootstrapping further resolves it into `const X = require('./dist/something')`
 */
tsConfigPaths.register({
  baseUrl: '.',
  paths: {
    'src/*': [
      'dist/*',
    ],
  },
});
