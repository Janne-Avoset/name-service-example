import bootstrap from './bootstrap';

async function start(): Promise<void> {
  await bootstrap();
}

start().catch((err) => {
  console.error(err);
  process.exit(1);
});
