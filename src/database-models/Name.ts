import mongoose, { Schema, Document } from 'mongoose';

export interface IName extends Document {
  name: string;
  amount: number;
}

const nameSchema: Schema = new Schema({
  name: {type: String, unique: true, required: true},
  amount: {type: Number, required: true}
}, { versionKey: false });

mongoose.models = {};
export default mongoose.model<IName>('Name', nameSchema);

