export enum Injectable {
  NAME_SERVICE = 'NameService',
  NAME_CONTROLLER = 'NameController',
  LOGGER = 'logger',
}
