import { Container } from 'typedi';

import initializeConfig from './config';
import initializeLogger, { Logger } from './logger';
import initializeServer from './server';
import initializeMongoose from './mongoose';
import { Injectable } from '../utils/enums';

// Typedi specific stuff:
import 'reflect-metadata'; // @Decorators won't work without this (https://github.com/typestack/typedi#usage-with-typescript)
// initialize dependency injections for services
import '../services/names/names.service';
import '../services/names/names.controller';


// Initialize the config, logger, db etc.
export default async function bootstrap(): Promise<void> {
  // Config needs to be loaded first, otherwise it won't be available for other modules
  await initializeConfig();
  await initializeLogger();
  await initializeMongoose();
  await initializeServer();


  const logger = Container.get<Logger>(Injectable.LOGGER);
  logger.info('Bootstrapping complete');
}
