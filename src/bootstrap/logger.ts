import winston from 'winston';
import { Container } from 'typedi';

import { Injectable } from 'src/utils/enums';

export type Logger = winston.Logger;

export default function initializeLogger(): void {
  let logger: Logger;

  if (process.env.NODE_ENV === 'production') {
    const format = winston.format.combine(
      winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
      winston.format.errors({ stack: true }),
      winston.format.splat(),
      winston.format.json(),
    );
    logger = winston.createLogger({
      level: global.gConfig.LOGGING_LEVEL,
      levels: winston.config.npm.levels,
      format,
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.cli(),
            winston.format.splat(),
          ),
        }),
        // In prod env errors will be written in to the error.log file
        new winston.transports.File({ filename: 'error.log', level: 'error' }),
        // All loggings will be written in to combined.log file
        new winston.transports.File({ filename: 'combined.log' }),
      ],
    });
  } else {
    const format = winston.format.combine(
      winston.format.errors({ stack: true }),
      winston.format.splat(),
      winston.format.json(),
      winston.format.prettyPrint(),
    );
    logger = winston.createLogger({
      level: global.gConfig.LOGGING_LEVEL,
      levels: winston.config.npm.levels,
      silent: process.env.NODE_ENV === 'test', // disable logging in testing
      format,
      transports: [
        // In dev env logs will be written only to a comsole
        new winston.transports.Console({ format }),
      ],
    });
  }
  Container.set(Injectable.LOGGER, logger);
}