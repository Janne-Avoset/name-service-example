import express, {Express} from 'express';
import cors from 'cors';
import helmet from 'helmet';
import { Container } from 'typedi';

import NamesController from '../services/names/names.controller';
import { Logger } from './logger';
import { Injectable } from '../utils/enums';


export default async function initializeServer(): Promise<void|Express> {
  // Inject Logger and NamesController
  const logger = Container.get<Logger>(Injectable.LOGGER);
  const nController = Container.get<NamesController>(Injectable.NAME_CONTROLLER);
  const PORT = global.gConfig.SERVER_PORT;

  // Check that PORT is configured
  if (!PORT) {
    logger.error('PORT not set, exiting');
    process.exit(1);
  }

  const app = express();

  // App Configs
  app.use(helmet());
  app.use(cors());
  app.use(express.json());

  // Routes
  app.use('/names', nController.router);

  // Create an express server
  app.listen(PORT, () => {
    logger.info(`Listening on port ${PORT}`);
  });

  // Supertest needs the server param to make requests. Return express server when in test env
  if (process.env.NODE_ENV == 'test') return app;
}