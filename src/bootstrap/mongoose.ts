import mongoose from 'mongoose';
import { Container } from 'typedi';

import { Logger } from 'src/bootstrap/logger';
import { Injectable } from 'src/utils/enums';

export default async function initializeMongoose(): Promise<void> {
  const auth = `${global.gConfig.MONGO_USER}:${global.gConfig.MONGO_PASSWORD}`;
  const mongoUrl = global.gConfig.MONGO_URI || `mongodb://${auth}@${global.gConfig.MONGO_HOST}/${global.gConfig.MONGO_DB}`;

  const logger = Container.get<Logger>(Injectable.LOGGER);
  logger.info(`Connecting to: ${mongoUrl.replace(auth, 'app:pwd')}`);

  // Connection params
  await mongoose.connect(mongoUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
    // don't autoIndex in production https://mongoosejs.com/docs/guide.html#indexes
    autoIndex: process.env.NODE_ENV !== 'production'
  });

  logger.info('Connected to database');
}