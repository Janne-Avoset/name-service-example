import * as dotenv from 'dotenv';

interface Config {
  LOGGING_LEVEL: 'error'|'warn'|'info'|'debug'|'silly';
  MONGO_USER: string;
  MONGO_PASSWORD: string;
  MONGO_HOST: string;
  MONGO_DB: string;
  MONGO_URI?: string;
  SERVER_PORT: number;
}

declare global {
  // eslint-disable-next-line
  namespace NodeJS {
    interface Global {
      gConfig: Config ;
    }
  }
}

export default async function initializeConfig(env: string = process.env.NODE_ENV): Promise<void> {
  dotenv.config();
  let config: Config;

  switch (env) {
    case 'production': {
      config = {
        LOGGING_LEVEL: 'warn',
        MONGO_USER: process.env.MONGO_USER,
        MONGO_PASSWORD: process.env.MONGO_PASSWORD,
        MONGO_HOST: process.env.MONGO_HOST,
        MONGO_DB: process.env.MONGO_DB,
        SERVER_PORT: parseInt(process.env.PORT as string, 10),
      };
      break;
    }
    case 'test': {
      config = {
        MONGO_USER: 'serveruser',
        MONGO_PASSWORD: 'testserverpass',
        MONGO_HOST: 'localhost',
        MONGO_DB: 'name-app-test',
        LOGGING_LEVEL: 'error',
        SERVER_PORT: 3001,
      };
      break;
    }
    default: {
      config = {
        LOGGING_LEVEL: 'silly',
        MONGO_USER: 'serveruser',
        MONGO_PASSWORD: 'devserverpass',
        MONGO_HOST: 'localhost',
        MONGO_DB: 'name-app-dev',
        SERVER_PORT: 3000,
      };
      break;
    }
  }

  global.gConfig = config;
}
