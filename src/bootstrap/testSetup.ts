// Typedi specific stuff:
import 'reflect-metadata'; // @Decorators won't work without this (https://github.com/typestack/typedi#usage-with-typescript)
// initialize dependency injections for services
import 'src/services/names/names.service';
import 'src/services/names/names.controller';

import {Express} from 'express';

import initializeConfig from './config';
import initializeLogger from './logger';
import initializeMongoose from './mongoose';
import initializeServer from './server';

export default async function testSetup(returnServer?:boolean): Promise<void|Express> {
  let app: Express;
  await initializeConfig();
  await initializeLogger();
  await initializeMongoose();
  // Express server is required by Supertest when testing app endpoints
  if (returnServer) {
    // When using testSetup, NODE_ENV is always 'test' and because of that return value of initializeServer cannot be void
    // eslint-disable-next-line
    // @ts-ignore
    app = await initializeServer();
    return app;
  }
}