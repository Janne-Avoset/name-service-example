import { Name } from './name.interface';

export type Names = Array<Name>;
