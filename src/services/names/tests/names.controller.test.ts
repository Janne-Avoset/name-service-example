import request from 'supertest';
import {Express} from 'express';

import Name from '../../../database-models/Name';
import testSetup from '../../../bootstrap/testSetup';
import {Names} from '../names.interface';


let server: Express;

const testData: Names = [
  {
    'name': 'Ville',
    'amount': 24
  },
  {
    'name': 'Anna',
    'amount': 6
  },
  {
    'name': 'Antti',
    'amount': 22
  }
];

beforeAll(async () => {
  // testSetup returns express server when it receives true parameter
  // eslint-disable-next-line
  // @ts-ignore
  server = await testSetup(true);
  await Name.insertMany(testData);
});

afterAll(async () => {
  await Name.collection.drop();
});

describe('GET /names', () => {

  it('should return status 200 and list of all the names ',  done => {
    request(server)
      .get('/names')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body).toEqual(expect.arrayContaining(testData)),
        done();
      });
  });

  it('should sort names alphabetically with query', done => {
    request(server)
      .get('/names?orderby=nameasc')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body[0].name).toMatch('Anna');
        expect(res.body[2].name).toMatch('Ville');
        done();
      });
  });

  it('should sort names reverse alphabetically', done => {
    request(server)
      .get('/names?orderby=nameDSC')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body[0].name).toMatch('Ville');
        expect(res.body[2].name).toMatch('Anna');
        done();
      });
  });

  it('should sort names by amount ASC', done => {
    request(server)
      .get('/names?orderby=amountASC')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body[0].amount).toEqual(6);
        expect(res.body[2].amount).toEqual(24);
        done();
      });
  });

  it('should sort names by amount DSC', done => {
    request(server)
      .get('/names?orderby=amountDSC')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body[0].amount).toEqual(24);
        expect(res.body[2].amount).toEqual(6);
        done();
      });
  });

  it('query parameters should not be case sensitive ', done => {
    request(server)
      .get('/names?orderby=NameASC')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body[0].name).toMatch('Anna');
        expect(res.body[2].name).toMatch('Ville');
        done();
      });
  });

  it('query parameters should not be case sensitive ', done => {
    request(server)
      .get('/names?orDerby=nameasc')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body[0].name).toMatch('Anna');
        expect(res.body[2].name).toMatch('Ville');
        done();
      });
  });
});

describe('GET /names/name/:name ', () => {
  it('should return Name object', done => {
    request(server)
      .get('/names/name/Ville')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(testData).toContainEqual(res.body);
        done();
      });
  });

  it('should not be case sensitive', done => {
    request(server)
      .get('/names/name/viLLe')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err) => {
        if (err) return done(err);
        done();
      });
  });

  it('should return error 404 and error message when name doesn\'t exist in db', done => {
    request(server)
      .get('/names/name/Vile')
      .expect('Content-Type', /text/)
      .expect(404)
      .end((err, res) => {
        if (err) return done(err);
        expect(res).not.toBe(undefined||null);
        done();
      });
  });
});

describe('Get /names/total', () => {
  it('should return JSON with numerical totalAmount value',done => {
    request(server)
      .get('/names/total')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        expect(res.body.totalAmount).not.toBeNaN();
        done();
      });
  });

  it('should sum up total amount correctly',done => {
    request(server)
      .get('/names/total')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err);
        const sumUp = testData.reduce((a,b) => {
          return a + b.amount;
        },0);

        expect(res.body.totalAmount).toBe(sumUp);
        done();
      });
  });
});