import express, {Request, Response} from 'express';
import {Container, Service} from 'typedi';

import NameService, { OrderBy } from './names.service';
import { Name } from './name.interface';
import { Names } from './names.interface';
import { Injectable } from 'src/utils/enums';
import { Logger } from 'src/bootstrap/logger';

/**
 * Controller definitions
 *
 * Supported endpoints:
   * GET names/
   * GET names?orderBy= nameASC | nameDSC | amountASC | amountDSC
   * GET names/name/:name
   * GET names/total
 *
 */

@Service(Injectable.NAME_CONTROLLER)
class NamesController {
  public router = express.Router();
  private logger = Container.get<Logger>(Injectable.LOGGER);
  private nService = Container.get<NameService>(Injectable.NAME_SERVICE);

  constructor() {
    this.initializeRoutes();
  }

  public initializeRoutes() : void {
    this.logger.info('Initializing routes');
    this.router.get('/', this.getAllNames);
    this.router.get('/name/:name', this.getName);
    this.router.get('/total', this.getAmountOfNames);
  }

  // TODO: Should orderby query key be case sensitive or not?
  getAllNames = async (req: Request, res:Response) : Promise<void> => {
    // If GET request has order by query defined
    if (req.query.orderby) {
      const order = req.query.orderby;
      try {
        const names: Names = await this.nService.findAllNamesAndSort(<OrderBy>order);
        res.status(200).send(names);
      } catch (e) {
        res.status(404).send(e.message);
      }
    } else {
      try {
        const names: Names = await this.nService.findAllNames();
        res.status(200).send(names);
      } catch (e) {
        res.status(404).send(e.message);
      }
    }
  };

  getName = async (req: Request, res: Response) : Promise<void> => {
    // Only first letter of the name should be in uppercase
    let searchName: string = req.params.name.toLowerCase();
    searchName = searchName.charAt(0).toUpperCase() + searchName.slice(1);

    try {
      const name: Name = await this.nService.findName(searchName);
      this.logger.debug(`Name service returned ${JSON.stringify(name)}`);
      res.status(200).send(name);
    } catch (e) {
      res.status(404).send(e.message);
      this.logger.error(e);
    }
  };

  getAmountOfNames = async (req: Request, res: Response) : Promise<void> => {
    try {
      const amount = await this.nService.findAmountOfAllNames() ;
      res.status(200).send(amount);
    } catch (e) {
      res.status(404).send(e.message);
    }
  };
}

export default NamesController;
