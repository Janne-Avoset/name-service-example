import {Service, Inject} from 'typedi';

import { Logger } from 'src/bootstrap/logger';
import { Injectable } from 'src/utils/enums';
import { Name as IName } from './name.interface';
import { Names as INames } from './names.interface';
import Name from 'src/database-models/Name';

// Typing options for orderBy parameter
export type OrderBy = 'nameasc' | 'namedsc' | 'amountasc' | 'amountdsc';
// Type for totalAmount() return value
type Total = { totalAmount: number };

@Service(Injectable.NAME_SERVICE)
class NameService {
  @Inject(Injectable.LOGGER)
  private logger: Logger;

  // Return all names
  public findAllNames = async (): Promise<INames> => {
    this.logger.info('Requesting all the names from the db');
    // Second parameter of .find() excludes _id keys from the return object
    const nameList: INames = await Name.find({},'-_id');
    this.logger.info(`Names returned from the db: ${nameList}`);

    return nameList;
  };

  // Return all names in wanted order
  // If parameter is not given or can't be found from options, nameASC is used.
  public findAllNamesAndSort = async (orderBy: OrderBy = 'nameasc'): Promise<INames> => {

    this.logger.info('Requesting all the names from the db');
    this.logger.info(`Names are sorted by ${orderBy}`);
    // Second parameter of .find() excludes _id keys from the return object
    const nameList: INames = await Name.find({}, '-_id');

    return nameList.sort((a, b) => {
      // Keep original order in case of same name
      if (a === b) return 0;
      // Based on orderBy parameter, sort to ascending or descending order
      switch (orderBy.toLowerCase()) {
        case 'nameasc':
          // Sort by names in ascending order
          return a.name.toUpperCase() < b.name.toUpperCase() ? -1 : 1;
        case 'namedsc':
          // Sort by names in descending order
          return a.name.toUpperCase() < b.name.toUpperCase() ? 1 : -1;
        case 'amountasc':
          // Sort by amount of names in ascending order
          return a.amount < b.amount ? -1 : 1;
        case 'amountdsc':
          // Sort by amount of names in descending order
          return a.amount < b.amount ? 1 : -1;
      }
    });
  };

  // Returns requested name if it exists in the list
  public findName = async (searchName: string): Promise<IName> => {
    this.logger.debug(`Requesting name ${searchName}`);
    // Second parameter of .find() excludes _id keys from the return object
    const name: IName = await Name.findOne({name:searchName}, '-_id');

    if (name) return name;
    else throw new Error('Name doesn\'t exist');
  };

  // Sums up the amount of all the names
  public findAmountOfAllNames = async (): Promise<Total> => {
    this.logger.debug('Requesting amount of names');
    let amountOfNames: number;

    await Name.find({},(err, res) => {
      // Using reduce to sum up amounts of names
      amountOfNames = res.reduce((a, {amount}) => {
        return a + amount;
      }, 0);
    });

    const total = {
      totalAmount: amountOfNames
    };

    return total;
  };
}

export default NameService;
