// TODO:
// Mongo docker image not supporting env variables inside JS init scripts, use .sh
// https://github.com/docker-library/mongo/issues/257
// https://jira.mongodb.org/browse/SERVER-4895

db.auth('root', 'devrootpass');

// Creating user to test db
db = db.getSiblingDB('name-app-test');
db.createUser(
  {
    user: 'serveruser',
    pwd: 'testserverpass',
    roles: [
      {
        role: 'readWrite',
        db: 'name-app-test'
      }
    ]
  }
);

// creating user to dev db
db = db.getSiblingDB('name-app-dev');
db.createUser(
    {
        user: 'serveruser',
        pwd: 'devserverpass',
        roles: [
            {
                role: 'readWrite',
                db: 'name-app-dev'
            }
        ]
    }
);

db.auth('serveruser','devserverpass');
// Inserting names.json
db.names.insert([
  {
    'name': 'Ville',
    'amount': 24
  },
  {
    'name': 'Anna',
    'amount': 6
  },
  {
    'name': 'Antti',
    'amount': 22
  },
  {
    'name': 'Sanna',
    'amount': 5
  },
  {
    'name': 'Mikko',
    'amount': 19
  },
  {
    'name': 'Minna',
    'amount': 5
  },
  {
    'name': 'Timo',
    'amount': 18
  },
  {
    'name': 'Satu',
    'amount': 5
  },
  {
    'name': 'Tuomas',
    'amount': 16
  },
  {
    'name': 'Tiina',
    'amount': 5
  },
  {
    'name': 'Tero',
    'amount': 15
  },
  {
    'name': 'Kati',
    'amount': 5
  },
  {
    'name': 'Sami',
    'amount': 15
  },
  {
    'name': 'Henna',
    'amount': 4
  },
  {
    'name': 'Mika',
    'amount': 12
  },
  {
    'name': 'Liisa',
    'amount': 4
  },
  {
    'name': 'Janne',
    'amount': 12
  },
  {
    'name': 'Paula',
    'amount': 4
  },
  {
    'name': 'Petri',
    'amount': 11
  },
  {
    'name': 'Suvi',
    'amount': 4
  }
]);

